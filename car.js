/**
 * Mongoose Schema for the Entity Car
 * @author Clark Jeria
 * @version 0.0.2
 */

var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var CarSchema   = new Schema({
    license: {
      type: String,
      required: true
    },
    model: {
      type: String,
      required: true
    },
    door_number: {
      type: Number,
      default: 4
    }
});

module.exports = mongoose.model('Car', CarSchema);
