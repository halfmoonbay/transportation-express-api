/** 
 * Mongoose Schema for the Entity Driver
 * @author Clark Jeria
 * @version 0.0.2
 */

var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

// definition of payment Schema
var PaySchema   = new Schema({
    accountType: {
      type : String,
      required : true,
      maxlength: 18
    },
    accountNumber : {
      type : Number,
      required : true,
      unique : true,
      maxlength: 18
    },
    expirationDate : {
      type: Number
    },
    accountName:{
      type: String,
      required: true,
      maxlength: 18
    },
    bank :{
        type : Number,
        ref : 'Driver',
        required: true
    },
    driver_id: String,
    passenger_id: String 

});
    
module.exports = mongoose.model('Pay', PaySchema);