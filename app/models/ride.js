/** 
 * Mongoose Schema for the Entity Ride
 * @author Clark Jeria
 * @version 0.0.2
 */

var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;
const utils = require('../utils');

// definition of ride Schema
var RideSchema  = new Schema({
    passenger: {
      type : Schema.Types.ObjectId,
      ref : 'Passenger',
      required : true
    },
    driver : {
      type: Schema.Types.ObjectId,
      ref : 'Driver',
      required : true
    },
    car : {
      type: Schema.Types.ObjectId,
      ref : 'Car',
      required: true
    },
    startPoint: {
        type : Schema.Types.Mixed,
        required : true
    },
    endPoint :  {
        type : Schema.Types.Mixed,
        required : true
    },
    requestTime : {
        type:Number,
        required: true
    },
    pickupTime :  {
        type:Number,
        required: true
    },
    dropoffTime :  {
        type:Number,
        required: true
    },
    rideType:{
        type: String,
        required: true,
        validate:[{
            validator: function(val){
                return val === 'ECONOMY' || val === 'PREMIUM' || val == 'EXECUTIVE';
            },
            msg: 'invalid ride type, you can only choose ECONOMY/ PREMIUM / EXECUTIVE',
            type: 'invalid'
        }]
    },
    status: {
        type : String,
        validate:[{
            validator: function(val){
                const validVal =['REQUESTED', 'AWAITING_DRIVER', 'DRIVE_ASSIGNED', 'IN_PROGRESS', 'ARRIVED', 'CLOSED'];
                return validVal.index(val)>=0;
            },
            msg: 'invaild ride status, you can only choose REQUESTED, AWAITING_DRIVER, DRIVE_ASSIGNED, IN_PROGRESS, ARRIVED, CLOSED',
            type:'invalid'
        }]
    },
    fee : Number,
    route:{
        lat: [Number],
        long: [Number]
    }

});
    
module.exports = mongoose.model('Ride', RideSchema);