/** 
 * Mongoose Schema for the Entity Passenger
 * @author Clark Jeria
 * @version 0.0.2
 */

var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;
const mongooseHidden = require('mongoose-hidden')();
const uniqueValidator = require('mongoose-unique-validator');

const utils = require('../utils');

// definition of passenger Schema

var PassengerSchema   = new Schema({
    firstName: {
        type : String,
        minlength:1,
        maxlength: 15,
        required : true
    },
    lastName : {
        type : String,
        minlength: 1,
        maxlength: 15,
        required : true
    },
    phoneNumber: {
        type: String,
        required: true,
        validate: [{
            validator: utils.validatePhoneNumber,
            msg: 'phoneNumber is invaild',
            type: 'invalid'
        }]
    },
    emailAddress: {
        type: String,
        required: true,
        unique: true,
        validate: [{
            validator: utils.validateEmail,
            msg: 'invaild email address',
            type: 'invalid'
        }, {
            validator: function(val) {
                return typeof val === 'string';
            },
            msg: 'TypeError: need to be a string',
            type : 'invalid'
        }]
    },
    password:{
        type : String, 
        hide : true,
        minlength: 6,
        maxlength: 18,
        required : true 
    }, 
    addressLine1 : {
        type: String,
        maxlength : 50
    },
    addressLine2 : {
        type: String,
        maxlength: 50
    },
    city : {
        type: String,
        maxlength: 50
    },
    state: {
        type :String, 
        minlength : 2
    },
    zip: {
        type: String,
        maxlength : 5
    }
});
PassengerSchema.plugin(mongooseHidden);
PassengerSchema.plugin(uniqueValidator);

module.exports = mongoose.model('Passenger', PassengerSchema);