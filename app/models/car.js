/**
 * Mongoose Schema for the Entity Car
 * @author Clark Jeria
 * @version 0.0.2
 */

var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

// definition of car schema
var CarSchema   = new Schema({
    make: {
      type: String,
      required : true,
      maxlength: 18
    }, 
    license: {
      type: String,
      required: true,
      unique: true,
      maxlength: 10

    },
    doorCount:{
      type: Number,
      minlength: 2,
      maxlength: 8,
      required: true
    } ,
    driver:{ 
      type: Schema.Types.ObjectId, 
      ref: 'Driver' 
    },
    model: {
      type: String,
      required: true,
      maxlength:18
    },
    brand: String, 
    color: String

});

module.exports = mongoose.model('Car', CarSchema);
