'use strict';

const conf = require('../config');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

/**
 * report error with http standard
 * 
 * @param {any} statusCode
 * @param {any} errorCode
 * @param {any} errorMsg
 * @param {any} res Express's response object
 */
function reportError(statusCode, errorCode, errorMsg, res) {
    const statusMsg = {
        200: 'OK',
        201: 'Created',
        204: 'No Content',
        400: 'Bad Request',
        404: 'Not Found',
        500: 'Internal Error',
        501: 'Not implemented'
    };

    res
        .status(statusCode)
        .json({
            status: statusCode,
            statusTxt: statusMsg[statusCode],
            errorCode,
            errorMsg,
            requestTime: Date.now()
        })
        .end();
}

/**
 * get a standard error info
 * 
 * @param {string} type Mongoose's error type
 * @param {any} source
 * @param {any} msg
 * @returns 
 * {
 *  errorCode: Number
 *  msg: String
 *  statusCode: Number
 * }
 */
function getErrorInfo(type, source,msg) {
    const errorType = {
        'invalid': {
            errorCode: 1001,
            msg: `${source} is not valid`,
            statusCode: 500
        },
        'required': {
            errorCode: 1002,
            msg: `${source} required`,
            statusCode: 400
        },
         'typeError': {
            errorCode: 1003,
            msg: `${source} type error`,
            statusCode: 400
        },
         'empty': {
            errorCode: 1004,
            msg: 'empty body',
            statusCode: 400
        },
        'format': {
            errorCode: 1005,
            msg: 'data format error',
            statusCode: 400
        },
        'invalidAttr': {
            errorCode: 1006,
            msg: `attribute ${source} is not valid`,
            statusCode: 400
        },
        'noId': {
            errorCode: 1007,
            msg: 'Id should not be provided',
            statusCode: 400
        },
        'duplicated': {
            errorCode: 1008,
            msg: `${source} duplicated`,
            statusCode: 400
        },
        'notUnique': {
            errorCode: 1009,
            msg: `${source} is not unique`,
            statusCode: 400
        },
        'lostId': {
            errorCode: 1010,
            msg: `resouce ${source} not found`,
            statusCode: 404
        },
        'ObjectId': {
            errorCode: 1011,
            msg: `resouce ${source} not found`,
            statusCode: 404
        },
        'maxlength': {
            errorCode: 1001, 
            msg: `longer than the maximum allowed length`,
            statusCode: 400
        },
        'minlength': {
            errorCode: 1001, 
            msg: `shorter than the minimum allowed length`,
            statusCode: 400
        },
    }

    let errorInfo;

    if (errorType[type]) {
        errorInfo = {
                errorCode: errorType[type].errorCode, 
                errorMsg: errorType[type].msg,
                statusCode: errorType[type].status
            };
    } else {
        errorInfo = {
                errorCode: 10000, 
                errorMsg: 'Unknown error.',
                statusCode: 400
            };
    }
  
    return errorInfo;
};

/**
 * Translate mongoose error to web server error message and send 
 * the message
 * 
 * @param {Mongoose Error} err Mongoose's error type
 * @param {Response} res HTTP response object
 * @param {string} source source of error
 * @returns
 */
function handleMongooError(err, res, source) {
    let errorInfo; 
    const detailErrors = err.errors;

    if (err && err.kind) {
        errorInfo = getErrorInfo(err.kind, err.path, err.message);
    } else if (detailErrors) {
        for(let errName in detailErrors) {
            if(detailErrors.hasOwnProperty(errName)) {
                errorInfo = getErrorInfo(detailErrors[errName].kind, errName, detailErrors[errName].message);
            }
        }
    } else {
        // custom error
        errorInfo = {
            statusCode: 400,
            errorCode: 1001,
            errorMessage: err.message
        }
    }
    reportError(errorInfo.statusCode, errorInfo.errorCode, errorInfo.errorMessage, res);
};

/**
 * Throw a missing value error.
 * 
 * @param {string} name of the missing field/param name.
 * @returns
 * @throw Error
 */    
function throwIfMissing(param) {
    throw new Error(`${param} required`);
};

/**
 * Validate the syntax of an email.
 * 
 * @param {string} email address.
 * @returns ture - email is valid, false othewise.
 */
function validateEmail(email) {
    var vali = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return vali.test(email);
}

/**
 * Validate the syntax of an phone number.
 * 
 * @param {string} phone number. e.g. 123-456-7890
 * @returns ture - the number is valid, false othewise.
 */
function validatePhoneNumber(phone) {
    var vali = /[\d]{3}-[\d]{3}-[\d]{4}/;
    return vali.test(phone);
}

/**
 * hash passord for saving in database
 * 
 * @param {string} plainPassword
 * @returns Promise Object
 */
function hashPassword(plainPassword) {
    return new Promise((resolve, reject) => {
        const saltRounds = 5;
        bcrypt.hash(plainPassword, saltRounds, (err, hash) => {
            if(err) {
                reject(err);
            }
            resolve(hash);
        });
    });
}

/**
 * veirty password with hash
 * 
 * @param {string} plainPassword
 * @param {string} hash
 * @returns Promise Object
 */
function verifyPassword(plainPassword, hash) {
    return new Promise((resolve, reject) => {
        bcrypt.compare(plainPassword, hash, (err, isEqual) => {
            if(err) {
                reject(err);
            }
            if(!isEqual) {
                reject({result: isEqual, message: 'wrong password'})
            }
            resolve(hash);
        });
    });
}

/**
 * Verify user login. If username and password are valid, generate
 * a token and send it back to user. Otherwise, send error message.
 * 
 * @param {object} user
 * example: 
 * {
 * username: 'user',
 * password: '123'
 * }
 * @param {object} options jwt options
 * @returns Promise Object
 */
function signUser(user, options) {
    return new Promise((resolve, reject) => {
        const secret = conf.secret;
        jwt.sign(user, secret, options, (err, token) => {
            if(err) {
                reject(err);
            }
            resolve(token);
        });
    });

}

/**
 * Verify token from the client. If succeed, call resolve function, 
 * otherwise call reject.
 * 
 * @param {string} token
 * @returns Promise object
 */
function verifyUser(token) {
    return new Promise((resolve, reject) => {
        const secret = conf.secret;
        jwt.verify(token, secret, (err, decoded) => {
            if(err) {
                reject(err);
            }
            resolve(decoded);
        });

    });
}

/** export module interface*/
module.exports = {
    reportError,
    handleMongooError,
    throwIfMissing,
    validateEmail,
    validatePhoneNumber,
    hashPassword,
    verifyPassword,
    verifyUser,
    signUser,
}
