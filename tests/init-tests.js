var supertest = require('supertest'),
assert = require('assert'),
app = require('../server.js');
var mongoose     = require('mongoose');

var initDrivers = [
  {
    "phoneNumber": "6503843598",
    "zip": 94024,
    "state": "CA",
    "city": "Los Altos",
    "addressLine1": "1740 Westbrook Ave",
    "password": "564925cec612a1111ec0a6094d6b95255d03e93c",
    "emailAddress": "cjeria@cmu.edu",
    "username": "cjeria",
    "licenseType": "C",
    "dateOfBirth": "2001-04-21T07:00:00.000Z",
    "lastName": "Jeria",
    "firstName": "Clark",
  },
  {
    "phoneNumber": "6503843598",
    "zip": 94024,
    "state": "CA",
    "city": "Los Altos",
    "addressLine1": "1740 Westbrook Ave",
    "password": "564925cec612a1111ec0a6094d6b95255d03e93c",
    "emailAddress": "cjeria@cmu.edu",
    "username": "cjeria",
    "licenseType": "C",
    "dateOfBirth": "2001-04-21T07:00:00.000Z",
    "lastName": "Jeria",
    "firstName": "David",
  },
  {
    "phoneNumber": "408-555-2737",
    "zip": 83874,
    "state": "AS",
    "city": "Anytown",
    "addressLine2": "",
    "addressLine1": "454 Main Street",
    "password": "anypwd",
    "emailAddress": "test-98989@example.com",
    "lastName": "Smith",
    "firstName": "John",
  },
];

for (var i=0; i<initDrivers.length; ++i) {
    // exports.init_drivers_for_test = function(done){
    // supertest(app)
    // .post('/api/drivers')
    // .send(initDrivers[i])
    // .expect(201)
    // .end(function(err, response){
    //     // console.log(err);
    //     // console.log(response.body);
    //     assert.ok(typeof response.body === 'object');
    //     driverOneId = response.body._id;
    //     return done();
    // });
    // };
}