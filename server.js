/** 
 * Example of RESTful API using Express and NodeJS
 * @author Shaojie Cai
 * @version 0.0.3
 */

/** BEGIN: Express Server Configuration */
var express    = require('express');
var app        = express();
var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 8080;

var mongoose    = require('mongoose');
var conf = require('./config');
// mongoose.connect('mongodb://apphw:cmusvsm@ds041496.mlab.com:41496/apphomework');
// mongoose.connect('mongodb://app_user:password@ds035826.mlab.com:35826/cmu_sv_app');
mongoose.connect(conf.database)

/** END: Express Server Configuration */

/** BEGIN: Express Routes Definition */
var utils = require('./app/utils');
var router = require('./routes/router');
var cars = require('./routes/cars');
var drivers = require('./routes/drivers');
var passengers = require('./routes/passengers');
var ride = require('./routes/ride');
var pay = require('./routes/payment');
var session = require('./routes/session');

/** Setup and sessions router do not need to verify token*/ 
app.use('/api', session);

/**  Install authentication function to express*/
app.use((req, res, next) => {
    const token = req.body.token || req.query.token || req.headers['x-access-token'];

    if(!token) { 
        utils.reportError(403, 1011, 'No token provided', res);
        return;
    }

    utils.verifyUser(token)
        .then(() => {
            next();
        })
        .catch((err) => {
            utils.reportError(403, 1011, 'Failed to authenticate token', res);
        });
});


app.use('/api', cars);
app.use('/api', drivers);
app.use('/api', passengers);
app.use('/api', router);
app.use('/api', ride);
app.use('/api', pay);
/** END: Express Routes Definition */

/** handle 404 resource */ 
app.use((req, res, next) => {
    utils.reportError(404, 1001, 'resource not found', res);
    next();
});

/** BEGIN: Express Server Start */
app.listen(port);
console.log('Service running on port ' + port);

module.exports = app;
/** END: Express Server Start */