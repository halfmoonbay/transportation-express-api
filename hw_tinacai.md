#Title
#APP Homework 

Error Code| Error Message |Relevant Resources |Parameters
  -------------|-------------------|-----------------------|---------
  1001|Invalid Resource name{0} given| All Resources|`0_Resource Name`
  1002|Given resource {0} does not exist|All Resources|`0_Resource Name`
  1003|Given resource {0} type error| All Resources|`0_Resource Name`
  1004|Empty body | All Resources| `None`
  1005|Data format error | All Resources|`None`
  1006|Invalid attribute value{0} |All Resources|`0_Attribute Value`
  1008|Attribute{0} is duplicate|All Resources|`0_Attribute Name`
  1009|Attribute{0} is not unique| All Resources|`0_Attribute Name`
  1010| Id not found | All resources| `None`


  

  

'invalid': {
            errorCode: 1001,
            msg: `${source} is not valid`,
            status: 500
        },
        'required': {
            errorCode: 1002,
            msg: `${source} required`,
            status: 400
        },
         'typeError': {
            errorCode: 1003,
            msg: `${source} type error`,
            status: 400
        },
         'empty': {
            errorCode: 1004,
            msg: 'empty body',
            status: 400
        },
        'format': {
            errorCode: 1005,
            msg: 'data format error',
            status: 400
        },
        'invalidAttr': {
            errorCode: 1006,
            msg: `attribute ${source} is not valid`,
            status: 400
        },
        'noId': {
            errorCode: 1007,
            msg: 'Id should not be provided',
            status: 400
        },
        'duplicated': {
            errorCode: 1008,
            msg: `${source} duplicated`,
            status: 400
        },
        'notUnique': {
            errorCode: 1009,
            msg: `${source} is not unique`,
            status: 400
        },
        'lostId': {
            errorCode: 1010,
            msg: `resouce ${source} not found`,
            status: 400